To build the latest runtime

1. Start a command line prompt (cmd.exe)
2. Change directory to project (i.e. c:\Users\jsmith\Documents\cygwin_win_x64)
3. Execute build.bat (be sure to answer any UAC prompts)
