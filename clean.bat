::
:: Clean the CYGWIN runtime folder of any existing build
::

rd /s /q bin
rd /s /q dev
rd /s /q etc
rd /s /q lib
rd /s /q sbin
rd /s /q temp
rd /s /q tmp
rd /s /q usr
rd /s /q var
del /f /q Cygwin-Terminal.ico
del /f /q Cygwin.bat
del /f /q Cygwin.ico
del /f /q setup.exe
